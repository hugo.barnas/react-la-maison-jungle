
import Banner from './Banner';
import Cart from './Cart';
import ShoppingList from './ShoppingList';
import logo from '../assets/logo.png'
// import QuestionForm from './QuestionForm';
import Footer from './Footer';
import { useEffect, useState } from 'react';


function App() {
  const savedCart = localStorage.getItem('cart')
  const [cart, updateCart] = useState(savedCart ? JSON.parse(savedCart) : [])
  const [isFooterShown, updateIsFooterShown] = useState([true])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart))
  }, [cart])

  return (
    <div>
      <Banner>
        <img src={logo} alt="La maison Jungle" className='lmj-logo' />
        <h1 className='lmj-title'>La Maison Jungle</h1>
      </Banner>
      <div className='lmj-layout-inner'>
        <Cart cart={cart} updateCart={updateCart} />
        <ShoppingList cart={cart} updateCart={updateCart} />
        {/* <QuestionForm /> */}
      </div>
      <button onClick={() => updateIsFooterShown(!isFooterShown)}>
        Cacher !
      </button>
      {isFooterShown && <Footer cart={cart} />}
    </div>
  );
}

export default App;
