import { useEffect, useState } from 'react'
import '../styles/Footer.css'

function Footer(cart) {
    const [inputValue, setInputValue] = useState('email')
    // useEffect(() => {
    //     console.log("cette alerte s'affiche à chaque rendu")
    // })

    // useEffect(() => {
    //     console.log(`Cette alerte s'affiche uniquement au premier rendu`)
    // }, [])

    // useEffect(() => {
    //     console.log(`Cette alerte s'affiche une première fois et quand mon panier est mise à jour`)
    // }, [cart])

    useEffect(() => {
        return () =>
            console.log(`Cette alerte s'affiche quand le footer est retiré du DOM`)
    })


    function handleInput(e) {
        setInputValue(e.target.value)
    }

    function handleBlur() {
        if (!inputValue.includes('@')) {
            alert("Attention, il n'y a pas d'@, ceci n'est pas une adresse valide.")
        }
    }


    return (
        <footer className='lmj-footer'>
            <div className='lmj-footer-elem'>
                Pour les passionné·e·s de plantes 🌿🌱🌵
            </div>
            <div>Laissez-nous votre mail : </div>
            <input
                placeholder="Entrez votre mail"
                onChange={handleInput}
                value={inputValue}
                onBlur={handleBlur}
            />
        </footer>
    )
}

export default Footer